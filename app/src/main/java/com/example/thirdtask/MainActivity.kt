package com.example.thirdtask

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.thirdtask.data.*
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    lateinit var phoneModel: PhoneViewModel
    lateinit var numbers : Array<String>
    private var db : PhoneDatabase? = null
    lateinit var dataList : List<PhoneData>
    lateinit var savedNum: String
    val CHANNEL_ID: String = "MyChannel"
    lateinit var notifyText: String
    private var nameList = ArrayList<String>()
    private var emailList = ArrayList<String>()
    lateinit var email : String

    @SuppressLint("ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        CONTEXT = this

        phoneModel = ViewModelProvider(this).get(PhoneViewModel::class.java)

        CoroutineScope(Dispatchers.IO).launch{
            contacts()
        }
        readNumber()
        val choiceBtn = findViewById<Button>(R.id.choice)
        choiceBtn.setOnClickListener {
           val intent = Intent(CONTEXT, PhoneList::class.java)
           startActivity(intent)
        }

        val showBtn = findViewById<Button>(R.id.show)
        showBtn.setOnClickListener {
            choiceAlertDialog()
        }

        val spBtn = findViewById<Button>(R.id.show_sp)
        spBtn.setOnClickListener {
            loadFromSP()
            Snackbar.make(it, "Сохранённый номер: ${savedNum}", Snackbar.LENGTH_SHORT)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_FADE)
                .show()
        }

        val notifyBtn = findViewById<Button>(R.id.show_notify)
        notifyBtn.setOnClickListener {
            loadFromSP()
            if (savedNum != "No number"){
                showDataInNotify(savedNum)
                createNotifyChannel()
                val builder =  NotificationCompat.Builder(CONTEXT, CHANNEL_ID)
                    .setContentTitle("Имя сохранённого номера")
                    .setContentText(notifyText)
                    .setSmallIcon(R.drawable.ic_baseline_phone_android_24)
                    .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                with(NotificationManagerCompat.from(CONTEXT)){
                    notify(0, builder.build())
                }

            }
            else Toast.makeText(CONTEXT, "Нет сохранённых номеров", Toast.LENGTH_SHORT).show()

        }
    }


    @SuppressLint("Range")
    private fun contacts(){
        if (checkPermissions(READ_CONTACTS)){
            val cursor = CONTEXT.contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                null,
                null,
                null,
                null
            )

            var index = 0

            val emCursor = CONTEXT.contentResolver.query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                null,
                null,
                null
            )
            emCursor?.let {
                while (it.moveToNext()){
                    nameList.add(it.getString(it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)))
                    emailList.add(it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)))
                }
            }

            cursor?.let {
                while (it.moveToNext()){
                    val name = it.getString(it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY))
                    val phone = it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    email = if (nameList.contains(name)){
                        emailList[nameList.indexOf(name)]
                    } else "No email"
                    val model = PhoneBook()
                    model.id = index+1
                    model.name = name
                    model.number = phone
                    model.email = email
                    phoneList.add(model)
                    index++
                }
            }
            cursor?.close()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ContextCompat.checkSelfPermission(CONTEXT, READ_CONTACTS) == PackageManager.PERMISSION_GRANTED)
            contacts()
    }


    companion object{
        var phoneList = ArrayList<PhoneBook>()
    }

    private fun choiceAlertDialog(){
        val builder = AlertDialog.Builder(CONTEXT)
        builder.setTitle(R.string.alert_string)
        builder.setItems(numbers){dialog, which ->
            showNumbersData(numbers[which])
        }
        builder.show()
    }

    private fun readNumber(){
        phoneModel.readData.observe(CONTEXT, { array ->
            val arr = Array(array.size,{"0"})
            var index = 0
            for (i in array){
                arr[index] = i.number
                index++
            }
            numbers = arr
        })
    }

    private fun showNumbersData(name : String){
        db = PhoneDatabase.getDatabase(CONTEXT)
        val phoneDao = db?.phoneDao()
        dataList = phoneDao!!.readForNumber(name)

        val name = findViewById<TextView>(R.id.name_main)
        val phone = findViewById<TextView>(R.id.phone_main)
        val email = findViewById<TextView>(R.id.email_main)

        name.text = dataList[0].name
        phone.text = dataList[0].number
        email.text = dataList[0].email
        saveInSP(dataList[0].number)
    }

    @SuppressLint("SetTextI18n")
    private fun showDataInNotify(name : String){
        db = PhoneDatabase.getDatabase(CONTEXT)
        val phoneDao = db?.phoneDao()
        dataList = phoneDao!!.readForNumber(name)
        notifyText = dataList[0].name
    }

    private fun saveInSP(number : String){
        val sharedPreferences = getSharedPreferences("SP", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putString("INSERTED_NUMBER",number)
        }.apply()

        Toast.makeText(CONTEXT, "Сохранено в SharedPreferences", Toast.LENGTH_LONG).show()
    }

    private fun loadFromSP(){
        val sharedPreferences = getSharedPreferences("SP", Context.MODE_PRIVATE)
        val savedNumber = sharedPreferences.getString("INSERTED_NUMBER", null)
        if (savedNumber != null){
            savedNum = savedNumber
        }
        else {
            savedNum = "No number"
            Toast.makeText(CONTEXT, "Нет сохранённых номеров", Toast.LENGTH_SHORT).show()
        }
    }


    private fun createNotifyChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = "Сообщение от программ"
            val descriptionText = "Сохранённый номер"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notifyManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notifyManager.createNotificationChannel(channel)
        }
    }

}