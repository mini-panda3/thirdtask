package com.example.thirdtask.data

import androidx.lifecycle.LiveData

class PhoneRepository(private val phoneDao : PhoneDao) {

    val readData : LiveData<List<PhoneData>> = phoneDao.readData()

    suspend fun addPhoneItem(item : PhoneData){
        phoneDao.addPhoneItem(item)
    }
}