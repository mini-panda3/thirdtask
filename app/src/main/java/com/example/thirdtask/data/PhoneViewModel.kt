package com.example.thirdtask.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class PhoneViewModel(application: Application): AndroidViewModel(application) {

    val readData : LiveData<List<PhoneData>>
    private val repository : PhoneRepository

    init {
        val phoneDao = PhoneDatabase.getDatabase(application).phoneDao()
        repository = PhoneRepository(phoneDao)
        readData = repository.readData
    }

    fun addPhoneItem(item : PhoneData){
        CoroutineScope(Dispatchers.IO).launch {
            repository.addPhoneItem(item)
        }
    }
}