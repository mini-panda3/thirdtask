package com.example.thirdtask.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "PhoneTable" , indices = arrayOf(Index(value = ["number"], unique = true)))
data class PhoneData (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var name: String = "",
    @ColumnInfo(name = "number") var number: String = "",
    var email: String = "")