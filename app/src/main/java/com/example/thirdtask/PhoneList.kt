package com.example.thirdtask

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.thirdtask.Adapters.Adapter
import com.example.thirdtask.data.OnClickPhoneItem
import com.example.thirdtask.data.PhoneBook
import com.example.thirdtask.data.PhoneData
import com.example.thirdtask.data.PhoneViewModel

class PhoneList : AppCompatActivity() {
    lateinit var phoneModel : PhoneViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_list)

        val recView = findViewById<RecyclerView>(R.id.phone_list)
        val linearLayoutManager = LinearLayoutManager(applicationContext)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recView.layoutManager = linearLayoutManager

        phoneModel = ViewModelProvider(this).get(PhoneViewModel::class.java)

        val adapter = Adapter(object : OnClickPhoneItem{
            override fun onClick(item: PhoneBook) {
                val name = item.name
                val phone = item.number
                val email = item.email
                val phoneData = PhoneData(0, name, phone, email)
                phoneModel.addPhoneItem(phoneData)
                Toast.makeText(this@PhoneList, "Запись успешно добавлена.", Toast.LENGTH_SHORT).show()
            }
        })

        phoneList = MainActivity.phoneList
        adapter.addData(phoneList)
        recView.adapter = adapter
    }

    companion object{
        var phoneList = ArrayList<PhoneBook>()
    }




}