package com.example.thirdtask

import android.Manifest
import android.os.Build
import androidx.core.content.ContextCompat
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

const val READ_CONTACTS = Manifest.permission.READ_CONTACTS

fun checkPermissions(permission : String): Boolean{
    return if (Build.VERSION.SDK_INT >= 20 &&
        ContextCompat.checkSelfPermission(CONTEXT, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CONTEXT, arrayOf(permission), 200)
            false
    }else true
}