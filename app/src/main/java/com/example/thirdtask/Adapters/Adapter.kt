package com.example.thirdtask.Adapters

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thirdtask.data.PhoneBook
import com.example.thirdtask.R
import com.example.thirdtask.data.OnClickPhoneItem
import com.example.thirdtask.databinding.PhoneContactBinding

class Adapter(private val onClickListener: OnClickPhoneItem) : RecyclerView.Adapter<Adapter.ItemHolder>() {
    var List = ArrayList<PhoneBook>()
    class ItemHolder(item : View): RecyclerView.ViewHolder(item){
        val binding = PhoneContactBinding.bind(item)
        fun bind(PhoneItem: PhoneBook) = with(binding){
            index.text = PhoneItem.id.toString()
            name.text = PhoneItem.name
            phone.text = PhoneItem.number
            email.text = PhoneItem.email
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.phone_contact, parent, false)
        return ItemHolder(view)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(List[position])
        holder.itemView.setOnClickListener {
            onClickListener.onClick(List[position])
        }
    }

    override fun getItemCount(): Int {
        return List.size
    }

    fun addData(item: ArrayList<PhoneBook>){
        List = item

    }
}