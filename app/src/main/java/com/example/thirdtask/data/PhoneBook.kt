package com.example.thirdtask.data

data class PhoneBook (
    var id: Int? = null,
    var name: String = "",
    var number: String = "",
    var email: String = "")