package com.example.thirdtask.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PhoneDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addPhoneItem(phone_data : PhoneData)

    @Query("SELECT * FROM PhoneTable ORDER BY id ASC")
    fun readData() : LiveData<List<PhoneData>>

    @Query("SELECT * FROM PhoneTable WHERE number == :number")
    fun readForNumber(number: String) : List<PhoneData>

}